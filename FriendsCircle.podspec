#
# Be sure to run `pod lib lint PROJECT.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FriendsCircle'
  s.version          = '0.1.0'
  s.summary          = '朋友圈功能界面封装.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/CoderAFI/FriendsCircle.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'CoderAFI' => 'developer_afi@163.com' }
  s.source           = { :git => 'https://gitlab.com/CoderAFI/FriendsCircle.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.prefix_header_file = 'FriendsCircle/FriendsCircle-Prefix.pch'
  s.public_header_files = 'FriendsCircle/FriendsCircle.h',
                          'FriendsCircle/PDCMomentsViewController.h',
                          'FriendsCircle/PDCPublishViewController.h',
                          'FriendsCircle/PDCLocalizeControl.h',
                          'FriendsCircle/View/PDCFriendsCircleView.h',
                          'FriendsCircle/Model/*.h'
  s.source_files = 'FriendsCircle/**/*.{h,m}'

  s.resource = 'FriendsCircle/Resources/FriendsCircle.bundle'

  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Masonry'
  s.dependency 'SDWebImage'
  s.dependency 'MJRefresh'
  s.dependency 'YYWebImage'
  s.dependency 'MLLabel'

end
