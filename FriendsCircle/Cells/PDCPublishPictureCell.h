//
//  PDCPublishPictureCell.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/28.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PDCPublishPictureCellDelegate <NSObject>

- (void)didSelectAddItem;

@end

@interface PDCPublishPictureCell : UITableViewCell

@property (nonatomic, weak) id<PDCPublishPictureCellDelegate> delegate;

- (void)bindModel:(NSArray *)images;

+ (CGFloat)estimatedHeightForModel:(NSArray *)images;

@end
