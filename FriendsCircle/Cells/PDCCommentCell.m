//
//  FXXCommentCell.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/10/13.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCCommentCell.h"
#import "NSString+PDC.h"
#import <Masonry/Masonry.h>

@interface PDCCommentCell()

@property (nonatomic, strong) UIView *topLine;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIView *highlightedContentView;

@end

@implementation PDCCommentCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self buildUI];
        [self layoutUI];
        [self styleUI];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    self.highlightedContentView.backgroundColor = highlighted ? UIColorFromRGB(0xE5E5EA) : [UIColor whiteColor];
    [super setHighlighted:highlighted animated:animated];
}

#pragma mark - View

- (void)buildUI {
    self.topLine = UIView.new;
    [self.contentView addSubview:self.topLine];

    self.highlightedContentView = UIView.new;
    [self.contentView addSubview:self.highlightedContentView];
    
    self.contentLabel = UILabel.new;
    [self.contentView addSubview:self.contentLabel];
}

- (void)layoutUI {
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset(57);
        make.top.equalTo(self.contentView);
        make.right.equalTo(self.contentView).with.offset(-15);
        make.bottom.equalTo(self.contentView).with.offset(-15);
    }];
    [self.contentLabel sizeToFit];
    
    [self.highlightedContentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentLabel);
        make.top.equalTo(self.contentLabel).with.offset(-5);
        make.right.equalTo(self.contentLabel);
        make.bottom.equalTo(self.contentLabel).offset(5);
    }];
    
    [self.topLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).with.offset(-57);
        make.top.equalTo(self.contentView);
        make.right.equalTo(self.contentView);
        make.height.equalTo(@0.5);
    }];
}

- (void)styleUI {
    self.topLine.backgroundColor = UIColorFromRGB(0xE8E8E8);
    self.topLine.hidden = YES;
    
    self.contentLabel.textColor = UIColorFromRGB(0x5682B2);
    self.contentLabel.font = [UIFont systemFontOfSize:14];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.lineBreakMode = NSLineBreakByCharWrapping;
    self.contentLabel.preferredMaxLayoutWidth = (UIScreen.mainScreen.bounds.size.width - 15 - 57);
    self.contentLabel.backgroundColor = [UIColor clearColor];
}

- (void)updateConstraints {
    [super updateConstraints];
}

#pragma mark - Data

- (void)bindModel:(PDCComment *)comment {
    if (!comment.showAllFlag) {
        self.contentLabel.attributedText = [comment getRealShowAttributeText];
    } else {
        self.contentLabel.text = comment.content;
    }
}

#pragma mark - Control

- (void)setShowTopLine:(BOOL)showTopLine {
    _showTopLine = showTopLine;
    self.topLine.hidden = !_showTopLine;
    if (_showTopLine) {
        [self.contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).with.offset(15);
        }];
    } else {
        [self.contentLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView);
        }];
    }
}

#pragma mark - Util

+ (CGFloat)heightForModel:(PDCComment *)comment withShowTopLine:(BOOL)showTopLine {
    CGFloat height = 0.0f;
    UIFont *contentFont = [UIFont systemFontOfSize:14];
    if (!comment.showAllFlag) {
        NSAttributedString *attributedText = [comment getRealShowAttributeText];
        CGRect rect = [attributedText boundingRectWithSize:CGSizeMake((SCREEN_WIDTH - 15 - 57), MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        height = rect.size.height + 16;
    } else {
        NSString *content = comment.content;
        CGSize size = [content sizeWithFont:contentFont];
        height = size.height + 16;
    }
    if (showTopLine) {
        height += 15;
    }
    return height;
}

@end
