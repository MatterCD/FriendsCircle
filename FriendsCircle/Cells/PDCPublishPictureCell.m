//
//  PDCPublishPictureCell.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/28.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCPublishPictureCell.h"
#import <Masonry/Masonry.h>

static NSString * const cellIdentifier = @"Publish_Pic_Cell";

@interface PDCPublishPictureCell()<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) UICollectionView *picCollectionView;

@property (nonatomic, strong) NSArray *pictures;

@end

@implementation PDCPublishPictureCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self buildUI];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

#pragma mark - View

- (void)buildUI {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.picCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    self.picCollectionView.backgroundColor = [UIColor whiteColor];
    self.picCollectionView.dataSource = self;
    self.picCollectionView.delegate = self;
    [self.picCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:cellIdentifier];
    [self.contentView addSubview:self.picCollectionView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.picCollectionView.frame = CGRectMake(15, 0, self.contentView.frame.size.width - 30, self.contentView.frame.size.height);
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.pictures.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    for (UIView *subView in cell.contentView.subviews) {
        [subView removeFromSuperview];
    }
    UIImage *image = self.pictures[indexPath.row];
    UIImageView *picImageView = [[UIImageView alloc] init];
    [cell.contentView addSubview:picImageView];
    [picImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(cell.contentView);
    }];
    picImageView.image = image;
    return cell;
}

#pragma mark - UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.pictures.count - 1)     {
        if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectAddItem)]) {
            [self.delegate didSelectAddItem];
        }
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = (SCREEN_WIDTH - 15 * 2 - 15 * 3) / 4;
    return CGSizeMake(width, width);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 15;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 15;
}

#pragma mark - Data

- (void)bindModel:(NSArray *)images {
    self.pictures = images;
    [self.picCollectionView reloadData];
}

#pragma mark - Util

+ (CGFloat)estimatedHeightForModel:(NSArray *)images {
    NSInteger count = images.count;
    NSInteger line = (count + 4 - 1) / 4;
    CGFloat size = (SCREEN_WIDTH - 75.0) / 4;
    CGFloat height = line * size + (line + 1) * 15.0;
    return height;
}

@end
