//
//  FriendsCircleViewController.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/15.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCMomentsViewController.h"
#import "PDCPublishViewController.h"
#import "PDCMomentDetailViewController.h"
#import "PDCMomentHeaderView.h"
#import "PDCMomentView.h"
#import "PDCMomentDigestView.h"
#import "PDCCommentCell.h"
#import "PDCChatInputBox.h"
#import "UIImage+PDC.h"
#import "UIView+PDC.h"
#import "UIScrollView+PDC.h"
#import "YYPhotoGroupView.h"
#import "PDCSudokuBoxView.h"
#import "NSString+PDC.h"
#import "NSDate+PDC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Masonry/Masonry.h>
#import <MJRefresh/MJRefresh.h>

static NSString * const PDCCOMMENTCELLID = @"PDC_CELL_COMMENT";
static NSString * const PDCMOMENTHEADERFOOTERVIEWID = @"PDC_HEADERFOOTERVIEW_MOMENT";
static NSString * const PDCMOMENTDIGESTHEADERFOOTERVIEWID = @"PDC_HEADERFOOTERVIEW_MOMENT_DIGEST";

@interface PDCMomentsViewController ()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, PDCMomentViewDelegate, PDCMomentDigestViewDelegate, PDCChatInputBoxDelegate>

@property (nonatomic, strong) PDCMomentHeaderView *coverImageHeaderView;

@property (nonatomic, assign) CGPoint oldContentOffset;

@property (nonatomic,strong) NSNumber *cellBottomLine;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@property (nonatomic, assign) NSInteger currentSelectedMomentIndex;
@property (nonatomic, assign) NSInteger currentSelectCommentIndex;

@property (nonatomic, strong) PDCChatInputBox *chatInputBox;
@property (nonatomic, assign) NSUInteger keyboardCalledCounter;
@property (nonatomic, assign) BOOL keyboardShowed;
@property (assign, nonatomic) NSInteger keyboardPresentFlag; // 键盘弹起的标志,弹出 = 1，收起 = 0

@end

@implementation PDCMomentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self buildUI];
    [self loadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self buildNavigationUI];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardFrameDidChange:)
                                                 name:UIKeyboardDidChangeFrameNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidChangeFrameNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - View

- (void)buildUI {
    [self buildFriendsCircleListView];
    [self buildRefreshUI];
    [self buildReplyKeyboardUI];
}

- (void)buildNavigationUI {
    switch (self.showMode) {
        case AllFriends:
        {
            UIImage *capturePhotoImage = [[UIImage pdcImageNamed:@"icon_nav_bar_camera"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            if (!self.containsInTabController) {
                self.title = PDCLocalizedString(@"friends_circle");
                self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:capturePhotoImage style:UIBarButtonItemStylePlain target:self action:@selector(navigateToAddNewMoment)];
            } else {
                self.tabBarController.navigationItem.title = PDCLocalizedString(@"friends_circle");
                self.tabBarController.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithImage:capturePhotoImage style:UIBarButtonItemStylePlain target:self action:@selector(navigateToAddNewMoment)]];
            }
        }
            break;
        case MineOwn:
        {
            if (!self.containsInTabController) {
                self.title = PDCLocalizedString(@"profile_album");
            } else {
                self.tabBarController.navigationItem.title = PDCLocalizedString(@"profile_album");
            }
        }
            break;
        default:
            break;
    }
}

- (void)buildFriendsCircleListView {
    self.friendsCircleListView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    self.friendsCircleListView.dataSource = self;
    self.friendsCircleListView.delegate = self;
    self.friendsCircleListView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.friendsCircleListView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.friendsCircleListView.showsVerticalScrollIndicator = NO;
    self.friendsCircleListView.estimatedRowHeight = 0;
    self.friendsCircleListView.estimatedSectionFooterHeight = 0;
    self.friendsCircleListView.estimatedSectionHeaderHeight = 0;
    self.friendsCircleListView.backgroundColor = [UIColor whiteColor];
    
    switch (self.showMode) {
        case AllFriends:
        {
            [self.friendsCircleListView registerClass:[PDCCommentCell class] forCellReuseIdentifier:PDCCOMMENTCELLID];
            [self.friendsCircleListView registerClass:[PDCMomentView class] forHeaderFooterViewReuseIdentifier:PDCMOMENTHEADERFOOTERVIEWID];
        }
            break;
        case MineOwn:
        {
            [self.friendsCircleListView registerClass:[PDCMomentDigestView class] forHeaderFooterViewReuseIdentifier:PDCMOMENTDIGESTHEADERFOOTERVIEWID];
        }
            break;
        default:
            break;
    }
    
    self.coverImageHeaderView = [[PDCMomentHeaderView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height / 2)];
    __weak typeof(self) weakSelf = self;
    _coverImageHeaderView.iconButtonClick = ^{
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(actionUserAvatarClick)]) {
            [weakSelf.delegate actionUserAvatarClick];
        }
    };
    _coverImageHeaderView.backgroundImageViewClick = ^ {
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(actionHeaderImageViewClick)]) {
            [weakSelf.delegate actionHeaderImageViewClick];
        }
    };
    self.friendsCircleListView.tableHeaderView = self.coverImageHeaderView;
    
    [self.view addSubview:self.friendsCircleListView];
    
    [self.friendsCircleListView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (void)buildRefreshUI {
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
    header.automaticallyChangeAlpha = YES;
    header.lastUpdatedTimeLabel.hidden = YES;
    [header beginRefreshing];
    self.friendsCircleListView.mj_header = header;
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    footer.automaticallyChangeAlpha = YES;
    self.friendsCircleListView.mj_footer = footer;
}

- (void)buildReplyKeyboardUI {
    self.chatInputBox = [[PDCChatInputBox alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 52)];
    self.chatInputBox.delegate = self;
    [self.chatInputBox setTextViewPlaceHolder:@"回复文章"];
    [self.view addSubview:self.chatInputBox];
}

- (UITapGestureRecognizer *)tapGesture {
    if (!_tapGesture) {
        _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTap)];
    }
    return _tapGesture;
}

- (void)userTap {
    self.cellBottomLine = nil;
    [self.chatInputBox keyboardDown];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.friendsCircleListItems.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (self.showMode) {
        case AllFriends:
        {
            PDCMoment *moment = self.friendsCircleListItems[section];
            return moment.comments.count;
        }
            break;
        case MineOwn:
        {
            return 0;
        }
            break;
        default:
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (self.showMode) {
        case AllFriends:
        {
            PDCCommentCell *commentCell = [tableView dequeueReusableCellWithIdentifier:PDCCOMMENTCELLID forIndexPath:indexPath];
            commentCell.selectionStyle = UITableViewCellSelectionStyleNone;
            NSInteger section = indexPath.section;
            NSInteger index = indexPath.row;
            PDCMoment *moment = self.friendsCircleListItems[section];
            PDCComment *comment = moment.comments[index];
            [commentCell bindModel:comment];
            commentCell.showTopLine = (index == 0);
            return commentCell;
        }
            break;
        case MineOwn:
        {
            return nil;
        }
            break;
        default:
            break;
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (self.showMode) {
        case AllFriends:
        {
            NSInteger section = indexPath.section;
            NSInteger index = indexPath.row;
            PDCMoment *moment = self.friendsCircleListItems[section];
            PDCComment *comment = moment.comments[index];
            return [PDCCommentCell heightForModel:comment withShowTopLine:(index == 0)];
        }
            break;
        case MineOwn: {
            return 0;
        }
            break;
        default:
            break;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    PDCMoment *moment = self.friendsCircleListItems[section];
    switch (self.showMode) {
        case AllFriends:
        {
            return [PDCMomentView heightForModel:moment];
        }
            break;
        case MineOwn:
        {
            return [PDCMomentDigestView heightForModel:moment];
        }
            break;
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    switch (self.showMode) {
        case AllFriends:
            return 0.5;
            break;
        case MineOwn:
            return 0.0;
            break;
        default:
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    switch (self.showMode) {
        case AllFriends:
        {
            UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 0.0f)];
            footerView.backgroundColor = [UIColor whiteColor];
            
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 0.5f)];
            lineView.backgroundColor = UIColorFromRGB(0xE8E8E8);
            [footerView addSubview:lineView];
            
            return footerView;
        }
            break;
        case MineOwn:
        {
            return nil;
        }
        default:
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    PDCMoment *moment = self.friendsCircleListItems[section];
    switch (self.showMode) {
        case AllFriends:
        {
            PDCMomentView *momentView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:PDCMOMENTHEADERFOOTERVIEWID];
            momentView.translatesAutoresizingMaskIntoConstraints = NO;
            momentView.delegate = self;
            [momentView bindModel:moment withIndex:section];
            return momentView;
        }
            break;
        case MineOwn:
        {
            BOOL needShowDate = [self mineOwnModeNeedShowDateOfMoment:moment];
            PDCMomentDigestView *momentDigestView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:PDCMOMENTDIGESTHEADERFOOTERVIEWID];
            momentDigestView.delegate = self;
            [momentDigestView bindModel:moment withIndex:section withShowDateFlag:needShowDate];
            return momentDigestView;
        }
            break;
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PDCMoment *moment = self.friendsCircleListItems[indexPath.section];
    PDCComment *comment = moment.comments[indexPath.row];
    if (comment.showAllFlag) {
        [self showAllCommentsOfMoment:moment atIndexPath:indexPath];
    } else {
        if (![self userLogin]) {
            return;
        }
        if (comment.canDel) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:PDCLocalizedString(@"alert_tips") message:PDCLocalizedString(@"alert_comment_delete") preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:PDCLocalizedString(@"alert_confirm") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(actionDeleteComment:ofMoment:)]) {
                    [self.delegate actionDeleteComment:comment  ofMoment:moment];
                }
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:PDCLocalizedString(@"alert_cancel") style:UIAlertActionStyleCancel handler:nil]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:alert animated:YES completion:nil];
            });
        } else {
            //            self.currentSelectedMessageIndex = indexPath.section;
            //            self.currentSelectCommentIndex = indexPath.row;
            //            NSString *commentUserName = comment.createUser.nickName;
            //            [self.chatInputBox setTextViewPlaceHolder:[NSString stringWithFormat:@"回复@%@:", commentUserName]];
            //
            //            CommentCell *commentCell = (CommentCell *)[self.discussListView cellForRowAtIndexPath:indexPath];
            //            CGRect cellFrame = [commentCell convertRect:commentCell.bounds toView:self.view];
            //            CGFloat cellBottomLine = CGRectGetMaxY(cellFrame);
            //            self.cellBottomLine = @(cellBottomLine);
            //            [self.chatInputBox keyboardUp];
        }
    }
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.keyboardShowed) {
        self.cellBottomLine = nil;
        [self.chatInputBox keyboardDown];
    }
}

#pragma mark - PDCMomentViewDelegate

- (void)momentView:(PDCMomentView *)momentView doDeleteMoment:(PDCMoment *)moment {
    if (![self userLogin]) {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionDeleteMoment:)]) {
        [self.delegate actionDeleteMoment:moment];
    }
}
-(void)momentView:(PDCMomentView *)momentView doFavouritesMoment:(PDCMoment *)moment{
    if (![self userLogin]) {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionFavouritesMoment:)]) {
        [self.delegate actionFavouritesMoment:moment];
    }
}
- (void)momentView:(PDCMomentView *)momentView doLikeMoment:(PDCMoment *)moment {
    if (![self userLogin]) {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionLikeMoment:)]) {
        [self.delegate actionLikeMoment:moment];
    }
    //    NSUInteger index = [self.discussListItems indexOfObject:message];
    //    if (message.currentUserIsLike) {
    //        FXXTopicFeedLikeRequest *likeRequest = [FXXTopicFeedLikeRequest new];
    //        likeRequest.feedType = 10;
    //        likeRequest.likeType = 2;
    //        likeRequest.contentID = message.discussId;
    //        [likeRequest sendRequestWithSuccess:^(id result) {
    //            if (!result) {
    //                [FXXToastView toastMessage:@"操作失败,请稍候再试"];
    //                return;
    //            }
    //            message.currentUserIsLike = NO;
    //            message.likeNum = @([message.likeNum integerValue] - 1);
    //            [messageHeaderView bindModel:message withIndex:index];
    //        } andFailure:^(id error) {
    //            [FXXToastView toastMessage:@"操作失败,请稍候再试"];
    //        }];
    //    } else {
    //        FXXTopicFeedLikeRequest *likeRequest = [FXXTopicFeedLikeRequest new];
    //        likeRequest.feedType = 10;
    //        likeRequest.likeType = 1;
    //        likeRequest.contentID = message.discussId;
    //        [likeRequest sendRequestWithSuccess:^(id result) {
    //            if (!result) {
    //                [FXXToastView toastMessage:@"操作失败,请稍候再试"];
    //                return;
    //            }
    //            message.currentUserIsLike = YES;
    //            message.likeNum = @([message.likeNum integerValue] + 1);
    //            [messageHeaderView bindModel:message withIndex:index];
    //        } andFailure:^(id error) {
    //            [FXXToastView toastMessage:@"操作失败,请稍候再试"];
    //        }];
    //    }
}

- (void)momentView:(PDCMomentView *)momentView doCommentMoment:(PDCMoment *)moment {
    if (![self userLogin]) {
        return;
    }
    NSUInteger sectionIndex = [self.friendsCircleListItems indexOfObject:moment];
    self.currentSelectedMomentIndex = sectionIndex;
    self.currentSelectCommentIndex = -1;
    NSString *createUserName = moment.createUser.nickName;
    [self.chatInputBox setTextViewPlaceHolder:[NSString stringWithFormat:@"%@@%@:", PDCLocalizedString(@"moment_comment"), createUserName]];
    if (moment.comments.count == 0) {
        CGRect messsageHeaderFrame = [momentView convertRect:momentView.bounds toView:self.view];
        CGFloat cellBottomLine = CGRectGetMaxY(messsageHeaderFrame);
        self.cellBottomLine = @(cellBottomLine);
    } else {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:moment.comments.count - 1 inSection:sectionIndex];
        [self.friendsCircleListView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
        PDCCommentCell *commentCell = (PDCCommentCell *) [self.friendsCircleListView cellForRowAtIndexPath:indexPath];
        CGRect commentCellFrame = [commentCell convertRect:commentCell.bounds toView:self.view];
        CGFloat cellBottomLine = CGRectGetMaxY(commentCellFrame);
        self.cellBottomLine = @(cellBottomLine);
    }
    [self.chatInputBox keyboardUp];
}

- (void)momentView:(PDCMomentView *)momentView doTapBoxView:(PDCSudokuBoxView *)boxView withMoment:(PDCMoment *)moment andImageIndex:(NSInteger)imageIndex {
    UIView *fromView = nil;
    NSMutableArray *items = [NSMutableArray new];
    for (NSUInteger i = 0; i < moment.imageUrls.count; i++) {
        UIImageView *imgView = boxView.imageViewItems[i];
        NSString *imageUrl = moment.imageUrls[i];
        
        YYPhotoGroupItem *item = [YYPhotoGroupItem new];
        item.thumbView = imgView;
        item.largeImageURL = [NSURL URLWithString:imageUrl];
        [items addObject:item];
        if (i == imageIndex) {
            fromView = imgView;
        }
    }
    YYPhotoGroupView *photoGroupView = [[YYPhotoGroupView alloc] initWithGroupItems:items];
    photoGroupView.blurEffectBackground = NO;
    [photoGroupView presentFromImageView:fromView toContainer:self.navigationController.view animated:YES completion:nil];
}

- (void)momentView:(PDCMomentView *)momentView doTapUserAvatarOfMoment:(PDCMoment *)moment {
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionClickUserAvatarOfMoment:)]) {
        [self.delegate actionClickUserAvatarOfMoment:moment];
    }
}

- (void)momentView:(PDCMomentView *)momentView doTapLikeUser:(NSString *)userId ofMoment:(PDCMoment *)moment {
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionClickLikeUser:ofMoment:)]) {
        [self.delegate actionClickLikeUser:userId ofMoment:moment];
    }
}

#pragma mark - PDCMomentDigestViewDelegate

- (void)digestMomentView:(PDCMomentDigestView *)momentDigestView doDeleteMoment:(PDCMoment *)moment {
    if (![self userLogin]) {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionDeleteMoment:)]) {
        [self.delegate actionDeleteMoment:moment];
    }
}

- (void)digestMomentView:(PDCMomentDigestView *)momentDigestView didSelectMoment:(PDCMoment *)moment atIndex:(NSInteger)index {
    [self navigateToMomentDetail:moment];
}

#pragma mark - PCChatInputBoxDelegate

- (void)chatInputBoxSendText:(NSString *)text {
    PDCMoment *selectedMoment = [self.friendsCircleListItems objectAtIndex:self.currentSelectedMomentIndex];
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionCommentMoment:withContent:)]) {
        [self.delegate actionCommentMoment:selectedMoment withContent:text];
    }
    [self.chatInputBox clearText];
    [self.chatInputBox keyboardDown];
    //    if (text.isEmpty) {
    //        [FXXToastView toastMessage:@"评论不能为空"];
    //        return;
    //    }
    //    if ([FXXTeamTrendsHelp isCommentTooLong:text]) {
    //        [FXXToastView toastMessage:[NSString stringWithFormat:@"评论不能超过%@个汉字",@([FXXTeamTrendsHelp maxCommentLength] / 2)]];
    //        return;
    //    }
}

- (void)chatInputBoxBeginEditing {
    self.keyboardPresentFlag ++;
}

- (void)chatInputBoxEndEditing {
    self.keyboardPresentFlag --;
}

#pragma mark - YYTextKeyboardObserver

- (void)keyboardWillShow:(NSNotification *)notification {
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    CGFloat chatViewTopY = self.view.bounds.size.height - (keyboardBounds.size.height + self.chatInputBox.frame.size.height);
    
    self.keyboardCalledCounter ++;
    
    if (self.keyboardCalledCounter >= 2) {
        self.friendsCircleListView.contentOffset = self.oldContentOffset;
    }
    
    self.oldContentOffset = self.friendsCircleListView.contentOffset;
    
    [UIView animateWithDuration:duration.doubleValue animations:^{
        CGFloat distance = [self.cellBottomLine floatValue] - chatViewTopY;
        CGPoint currentOffset = self.friendsCircleListView.contentOffset;
        CGFloat finalYPosition = currentOffset.y + distance;
        if (finalYPosition > 0) {
            self.friendsCircleListView.contentOffset = CGPointMake(currentOffset.x, finalYPosition);
        }
        self.chatInputBox.y = chatViewTopY;
    } completion:^(BOOL finished) {
        self.keyboardCalledCounter --;
        [self.view addGestureRecognizer:self.tapGesture];
        self.keyboardShowed = YES;
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    CGFloat chatViewTopY = self.view.bounds.size.height;
    [UIView animateWithDuration:duration.doubleValue animations:^{
        if (self.friendsCircleListView.contentOffset.y > (self.friendsCircleListView.contentSize.height - self.friendsCircleListView.frame.size.height)) {
            [self.friendsCircleListView scrollToBottomAnimated:YES];
        }
        self.chatInputBox.y = chatViewTopY;
    } completion:^(BOOL finished) {
        [self.view removeGestureRecognizer:self.tapGesture];
        self.keyboardShowed = NO;
    }];
}

- (void)keyboardFrameDidChange:(NSNotification *)notification {
    CGSize keyboardSize = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    CGFloat keyboardOriginY = self.view.frame.size.height - keyboardSize.height;
    CGFloat chatViewTopY = self.chatInputBox.frame.origin.y + self.chatInputBox.frame.size.height - keyboardOriginY;
    CGPoint currentOffset = self.friendsCircleListView.contentOffset;
    currentOffset.y += chatViewTopY;
    
    if (self.keyboardPresentFlag == 1) {
        [UIView animateWithDuration:duration.doubleValue delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.chatInputBox.y -= chatViewTopY;
            if (currentOffset.y > 0) {
                self.friendsCircleListView.contentOffset = currentOffset;
            }
        } completion:nil];
    }
}

#pragma mark -
#pragma mark - Data

- (void)initData {
    self.friendsCircleListItems = [NSMutableArray<PDCMoment *> new];
}

- (void)loadData {
    if (self.delegate && [self.delegate respondsToSelector:@selector(dataLoadMoments:)]) {
        [self.delegate dataLoadMoments:NO];
    }
}

- (void)loadNewData {
    [self loadData];
}

- (void)loadMoreData {
    if (self.delegate && [self.delegate respondsToSelector:@selector(dataLoadMoments:)]) {
        [self.delegate dataLoadMoments:YES];
    }
}

- (void)updateListViewHeader:(NSString *)backgroundImageUrl userAvatarImage:(NSString *)avatarImageUrl andUserName:(NSString *)username {
    [self updateListViewHeaderImageView:backgroundImageUrl];
    [self updateUserAvatarImageView:avatarImageUrl];
    [self updateUserNameView:username];
}

- (void)updateListViewHeaderImageView:(NSString *)backgroundImageUrl {
    [self.coverImageHeaderView.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:backgroundImageUrl] placeholderImage:[UIImage pdcImageNamed:@"AlbumCover"]];
}

- (void)updateUserAvatarImageView:(NSString *)avatarImageUrl {
    [self.coverImageHeaderView.iconView sd_setImageWithURL:[NSURL URLWithString:avatarImageUrl] placeholderImage:[UIImage pdcImageNamed:@"user_default"]];
}

- (void)updateUserNameView:(NSString *)username {
    self.coverImageHeaderView.nameLabel.text = username;
}

- (void)addMoments:(NSArray<PDCMoment *> *)moments withRealodFlag:(BOOL)reload {
    if (reload) {
        [self.friendsCircleListView.mj_header endRefreshing];
        [self.friendsCircleListView.mj_footer resetNoMoreData];
        [self.friendsCircleListItems removeAllObjects];
    } else {
        [self.friendsCircleListView.mj_footer endRefreshing];
        if (moments.count == 0) {
            [self.friendsCircleListView.mj_footer endRefreshingWithNoMoreData];
        }
    }
    if (moments == nil) {
        return;
    }
    [self.friendsCircleListItems addObjectsFromArray:moments];
    [self.friendsCircleListView reloadData];
}

- (void)insertMoment:(PDCMoment *)moment atIndex:(NSUInteger)index {
    [self.friendsCircleListItems insertObject:moment atIndex:0];
    [self.friendsCircleListView reloadData];
    //    BOOL isEmpty = (self.friendsCircleListItems.count == 0);
    //    if (isEmpty) {
    //        [self.friendsCircleListView reloadData];
    //    } else {
    //        CGPoint tableViewOffset = self.friendsCircleListView.contentOffset;
    //        CGFloat heightForSection = 0.0f;
    //
    //        [self beginTableViewDynamicUpdate];
    //        [self insertDiscussListViewWithSection:1];
    //        [self endTableViewDynamicUpdate];
    //
    //        heightForSection += [self heightForSectionAtSectionIndex:1];
    //        tableViewOffset.y += heightForSection;
    //
    //        [self keepTableViewScrollPosition:tableViewOffset];
    //    }
}

- (void)updateMoment:(PDCMoment *)moment likeStatus:(BOOL)like {
    if (like) {
        moment.currentUserIsLike = YES;
        moment.likeNum = @([moment.likeNum integerValue] + 1);
    } else {
        moment.currentUserIsLike = NO;
        moment.likeNum = @([moment.likeNum integerValue] - 1);
    }
    NSInteger index = [self.friendsCircleListItems indexOfObject:moment];
    [self reloadDiscussListViewAtSection:index];
}

- (void)deleteMoment:(PDCMoment *)moment {
    NSUInteger index = [self.friendsCircleListItems indexOfObject:moment];
    CGPoint tableViewOffset = self.friendsCircleListView.contentOffset;
    CGFloat heightForSection = 0.0f;
    
    heightForSection -= [self heightForSectionAtSectionIndex:index];
    tableViewOffset.y += heightForSection;
    
    [self.friendsCircleListItems removeObjectAtIndex:index];
    
    [self beginTableViewDynamicUpdate];
    [self deleteDiscussListViewWithSection:index];
    [self endTableViewDynamicUpdate];
    
    [self keepTableViewScrollPosition:tableViewOffset];
}

- (void)addComment:(PDCComment *)comment {
    PDCMoment *moment = self.friendsCircleListItems[self.currentSelectedMomentIndex];
    [moment.comments addObject:comment];
    NSIndexPath *commentIndexPath = [NSIndexPath indexPathForRow:moment.comments.count - 1 inSection:self.currentSelectedMomentIndex];
    
    __block CGPoint tableViewOffset = self.friendsCircleListView.contentOffset;
    CGFloat heightForNewRows = 0.0f;
    
    [self beginTableViewDynamicUpdate];
    [self insertDiscussListViewWithIndexPaths:@[commentIndexPath]];
    [self endTableViewDynamicUpdate];
    
    heightForNewRows += [self heightForCellAtIndexPath:commentIndexPath];
    tableViewOffset.y += heightForNewRows;
    
    [self keepTableViewScrollPosition:tableViewOffset];
    
    if (self.currentSelectCommentIndex != -1) {
        [self.friendsCircleListView scrollToRowAtIndexPath:commentIndexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
    }
}

- (void)showAllCommentsOfMoment:(PDCMoment *)moment atIndexPath:(NSIndexPath *)indexPath {
    //    FXXEvaluateDetailCommentsRequest *commentsRequest = [FXXEvaluateDetailCommentsRequest new];
    //    commentsRequest.discussId = message.discussId;
    //    commentsRequest.type = 10;
    //    commentsRequest.offset = 2;
    //    commentsRequest.pageSize = [message.commentNum integerValue] - 2;
    //
    //    [commentsRequest sendRequestWithSuccess:^(id result) {
    //        if ([result isKindOfClass:[NSArray class]]) {
    //            message.showAll = YES;
    //            [self beginTableViewDynamicUpdate];
    //
    //            NSUInteger deleteRow = (message.comments.count - 1);
    //            [message.comments removeObjectAtIndex:deleteRow];
    //            NSIndexPath *deleteIndexPath = [NSIndexPath indexPathForRow:deleteRow inSection:indexPath.section];
    //            [self deleteDiscussListViewWithIndexPaths:@[deleteIndexPath]];
    //
    //            [message.comments addObjectsFromArray:result];
    //            NSMutableArray *insertIndexPathArray = [NSMutableArray array];
    //            for (NSUInteger row = 2; row < message.commentNum.integerValue; ++row) {
    //                NSIndexPath *insertIndexPath = [NSIndexPath indexPathForRow:row inSection:indexPath.section];
    //                [insertIndexPathArray appendObject:insertIndexPath];
    //            }
    //            [self insertDiscussListViewWithIndexPaths:insertIndexPathArray];
    //
    //            [self endTableViewDynamicUpdate];
    //        }
    //    } andFailure:^(id error) {
    //        NSError *localError = (NSError *)error;
    //        [FXXToastView toastMessage:localError.localizedDescription];
    //    }];
}

- (void)deleteComment:(PDCComment *)comment ofMoment:(PDCMoment *)moment {
    NSInteger section = [self.friendsCircleListItems indexOfObject:moment];
    NSInteger row = [moment.comments indexOfObject:comment];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
    
    CGPoint tableViewOffset = self.friendsCircleListView.contentOffset;
    CGFloat heightForNewRows = 0.0f;
    
    NSIndexPath *deleteIndexPath = indexPath;
    heightForNewRows -= [self heightForCellAtIndexPath:deleteIndexPath];
    tableViewOffset.y += heightForNewRows;
    
    [moment.comments removeObjectAtIndex:indexPath.row];
    
    [self beginTableViewDynamicUpdate];
    [self reloadDiscussListViewAtSection:indexPath.section];
    [self endTableViewDynamicUpdate];
    
    [self keepTableViewScrollPosition:tableViewOffset];
}

- (void)deleteComment:(NSString *)commentId atIndexPath:(NSIndexPath *)indexPath {
    
    //    FXXCommentDeleteRequest *commentDeleteRequest = [FXXCommentDeleteRequest new];
    //    commentDeleteRequest.commentID = commentId;
    //    [commentDeleteRequest sendRequestWithCompletion:^(NSDictionary * result, NSError *error) {
    //        if (!error) {
    //            FXXMessageInfoEntity *message = self.discussListItems[indexPath.section];
    //            message.commentNum = @(message.commentNum.integerValue - 1);
    //
    //            if (message.showAll || message.commentNum.integerValue < 2) {
    //                CGPoint tableViewOffset = self.discussListView.contentOffset;
    //                CGFloat heightForNewRows = 0.0f;
    //
    //                NSIndexPath *deleteIndexPath = indexPath;
    //                heightForNewRows -= [self heightForCellAtIndexPath:deleteIndexPath];
    //                tableViewOffset.y += heightForNewRows;
    //
    //                [message.comments removeObjectAtIndex:indexPath.row];
    //
    //                [self beginTableViewDynamicUpdate];
    //                [self reloadDiscussListViewAtSection:indexPath.section];
    //                [self endTableViewDynamicUpdate];
    //
    //                [self keepTableViewScrollPosition:tableViewOffset];
    //            } else if (message.commentNum.integerValue == 2) {
    //                [message.comments removeObjectAtIndex:indexPath.row];
    //                FXXEvaluateDetailCommentsRequest *commentsRequest = [FXXEvaluateDetailCommentsRequest new];
    //                commentsRequest.discussId = message.discussId;
    //                commentsRequest.type = 10;
    //                commentsRequest.offset = 0;
    //                commentsRequest.pageSize = 2;
    //                [commentsRequest sendRequestWithSuccess:^(id result) {
    //                    if ([result isKindOfClass:[NSArray class]]) {
    //                        [message.comments removeAllObjects];
    //                        [message.comments addObjectsFromArray:result];
    //                        [self beginTableViewDynamicUpdate];
    //                        [self reloadDiscussListViewAtSection:indexPath.section];
    //                        [self endTableViewDynamicUpdate];
    //                    }
    //                } andFailure:^(id error) {
    //                    NSError *localError = (NSError *)error;
    //                    [FXXToastView toastMessage:localError.localizedDescription];
    //                }];
    //            } else {
    //                [message.comments removeObjectAtIndex:indexPath.row];
    //                FXXEvaluateDetailCommentsRequest *commentsRequest = [FXXEvaluateDetailCommentsRequest new];
    //                commentsRequest.discussId = message.discussId;
    //                commentsRequest.type = 10;
    //                commentsRequest.offset = 2;
    //                commentsRequest.pageSize = 1;
    //                [commentsRequest sendRequestWithSuccess:^(id result) {
    //                    if ([result isKindOfClass:[NSArray class]]) {
    //                        FXXCommentInfoEntity *leftFirstComment = result[0];
    //                        [message.comments insertObject:leftFirstComment atIndex:message.comments.count - 2];
    //                        [message processLimitedComment:YES];
    //                        [self beginTableViewDynamicUpdate];
    //                        [self reloadDiscussListViewAtSection:indexPath.section];
    //                        [self endTableViewDynamicUpdate];
    //                    }
    //                } andFailure:^(id error) {
    //                    NSError *localError = (NSError *)error;
    //                    [FXXToastView toastMessage:localError.localizedDescription];
    //                }];
    //            }
    //        } else {
    //            [FXXToastView toastMessage:error.localizedDescription];
    //        }
    //    }];
}

#pragma mark - Control

#pragma mark - Control Navigation

- (void)navigateToAddNewMoment {
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionAddNewMoment)]) {
        [self.delegate actionAddNewMoment];
    }
}

- (void)navigateToMomentDetail:(PDCMoment *)moment {
    PDCMomentDetailViewController *momentDetailViewController = [PDCMomentDetailViewController new];
    momentDetailViewController.moment = moment;
    [self.navigationController pushViewController:momentDetailViewController animated:YES];
}

#pragma mark -
#pragma mark - Util

#pragma mark - Discuss Util

- (void)insertDiscussListViewWithSection:(NSInteger)sectionIndex {
    NSIndexSet *sections = [NSIndexSet indexSetWithIndex:sectionIndex];
    [self.friendsCircleListView insertSections:sections withRowAnimation:UITableViewRowAnimationNone];
}

- (void)deleteDiscussListViewWithSection:(NSInteger)sectionIndex {
    NSIndexSet *sections = [NSIndexSet indexSetWithIndex:sectionIndex];
    [self.friendsCircleListView deleteSections:sections withRowAnimation:UITableViewRowAnimationNone];
}

- (void)reloadDiscussListViewAtSection:(NSInteger)sectionIndex {
    NSIndexSet *sections = [NSIndexSet indexSetWithIndex:sectionIndex];
    [self.friendsCircleListView reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
}

- (void)reloadDiscussListViewAtSections:(NSIndexSet *)sections {
    [self.friendsCircleListView reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
}

- (void)insertDiscussListViewWithIndexPaths:(NSArray *)indexPaths {
    [self.friendsCircleListView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (void)deleteDiscussListViewWithIndexPaths:(NSArray *)indexPaths {
    [self.friendsCircleListView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (void)reloadDiscussListViewIndexPaths:(NSArray<NSIndexPath *> *)indexPaths {
    [self.friendsCircleListView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
}

- (void)beginTableViewDynamicUpdate {
    [UIView setAnimationsEnabled:NO];
    [self.friendsCircleListView beginUpdates];
}

- (void)endTableViewDynamicUpdate {
    [self.friendsCircleListView endUpdates];
    [UIView setAnimationsEnabled:YES];
}

- (CGFloat)heightForCellAtIndexPath:(NSIndexPath *)indexPath {
    PDCMoment *moment = self.friendsCircleListItems[indexPath.section];
    PDCComment *comment = moment.comments[indexPath.row];
    return [PDCCommentCell heightForModel:comment withShowTopLine:(indexPath.row == 0)];
}

- (CGFloat)heightForSectionAtSectionIndex:(NSInteger)sectionIndex {
    PDCMoment *moment = self.friendsCircleListItems[sectionIndex];
    return [PDCMomentView heightForModel:moment];
}

- (void)keepTableViewScrollPosition:(CGPoint)tableViewOffset {
    // TOP
    if (self.friendsCircleListView.contentSize.height <= self.friendsCircleListView.frame.size.height) {
        CGPoint off = self.friendsCircleListView.contentOffset;
        off.y = 0 - self.friendsCircleListView.contentInset.top;
        [self.friendsCircleListView setContentOffset:off animated:YES];
        return;
    }
    // BOTTOM
    if (tableViewOffset.y > (self.friendsCircleListView.contentSize.height - self.friendsCircleListView.frame.size.height)) {
        CGPoint off = self.friendsCircleListView.contentOffset;
        off.y = self.friendsCircleListView.contentSize.height - self.friendsCircleListView.bounds.size.height + self.friendsCircleListView.contentInset.bottom;
        [self.friendsCircleListView setContentOffset:off animated:YES];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.friendsCircleListView setContentOffset:tableViewOffset animated:NO];
        });
    }
}

- (BOOL)userLogin {
    if (self.delegate && [self.delegate isUserLoggedIn]) {
        return [self.delegate isUserLoggedIn];
    } else {
        return NO;
    }
    return YES;
}

- (BOOL)mineOwnModeNeedShowDateOfMoment:(PDCMoment *)moment {
    NSMutableArray<PDCMoment *> *groupedMoments = [NSMutableArray new];
    for (PDCMoment *momentObj in self.friendsCircleListItems) {
        NSString *momentObjDateStr = [NSDate dateStringFromTimestamp:momentObj.createTimestamp.doubleValue withFormat:@"yyyy-MM-dd"];
        NSString *momentDateString = [NSDate dateStringFromTimestamp:moment.createTimestamp.doubleValue withFormat:@"yyyy-MM-dd"];
        if ([momentObjDateStr isEqualToString:momentDateString]) {
            [groupedMoments addObject:momentObj];
        }
    }
    return [groupedMoments indexOfObject:moment] == 0;
}

@end

