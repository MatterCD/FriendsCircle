//
//  PDCComment.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/15.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCComment.h"

@implementation PDCComment

- (NSAttributedString *)getRealShowAttributeText {
    NSString *username = self.createUser.nickName;
    NSString *contentText = self.content;
    if (self.parentComment) {
        NSString *parentCommentUserName = self.parentComment.createUser.nickName;
        NSString *replyText = @"回复";
        
        NSString *finalContent = [NSString stringWithFormat:@"%@ %@ %@：%@", username, replyText, parentCommentUserName, contentText];
        NSRange usernameRange = [finalContent rangeOfString:username];
        NSRange replyTextRange = [finalContent rangeOfString:replyText];
        NSRange parentCommentUserNameRange = [finalContent rangeOfString:parentCommentUserName];
        NSRange contentRange = [finalContent rangeOfString:contentText];
        
        NSMutableAttributedString *finalContentAttributeText = [[NSMutableAttributedString alloc] initWithString:finalContent];
        [finalContentAttributeText addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, finalContent.length)];
        [finalContentAttributeText addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x5682B2) range:usernameRange];
        [finalContentAttributeText addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x464646) range:replyTextRange];
        [finalContentAttributeText addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x5682B2) range:parentCommentUserNameRange];
        [finalContentAttributeText addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x464646) range:contentRange];
        return finalContentAttributeText;
    } else {
        NSString *finalContent = [NSString stringWithFormat:@"%@：%@", username, contentText];
        NSMutableAttributedString *finalContentAttributeText = [self attributedString:finalContent withString:username prefixColor:UIColorFromRGB(0x5682B2) suffixColor:UIColorFromRGB(0x464646) prefixFont:[UIFont systemFontOfSize:14] suffixFont:[UIFont systemFontOfSize:14]];
        return finalContentAttributeText;
    }
}

- (NSMutableAttributedString *)attributedString:(NSString *)string withString:(NSString *)keyString
                                    prefixColor:(UIColor *)prefixColor suffixColor:(UIColor *)suffixColor
                                     prefixFont:(UIFont *)prefixFont suffixFont: (UIFont *)suffixFont {
    NSRange prefixRange = [string rangeOfString:keyString];
    NSRange suffixRange = NSMakeRange(prefixRange.length, string.length - prefixRange.length);
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    [attributedString addAttribute:NSForegroundColorAttributeName value:prefixColor range:prefixRange];
    [attributedString addAttribute:NSFontAttributeName value:prefixFont range:prefixRange];
    [attributedString addAttribute:NSForegroundColorAttributeName value:suffixColor range:suffixRange];
    [attributedString addAttribute:NSFontAttributeName value:suffixFont range:suffixRange];
    return attributedString;
}

@end
