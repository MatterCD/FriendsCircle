//
//  PDCLike.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/3.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDCUser.h"

@interface PDCLike : NSObject

@property (nonatomic, strong) NSNumber *likeId;
@property (nonatomic, strong) NSNumber *createTimestamp;
@property (nonatomic, strong) NSNumber *updateTimestap;
@property (nonatomic, strong) PDCUser *createdUser;

@property (nonatomic, copy) NSAttributedString *attributedContent;

@end
