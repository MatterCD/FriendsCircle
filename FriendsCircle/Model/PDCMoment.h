//
//  Message.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/15.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDCLike.h"
#import "PDCComment.h"
#import "PDCUser.h"

@interface PDCMoment : NSObject

@property (nonatomic, copy) NSString *momentId;                         // 朋友圈新闻编号
@property (nonatomic, copy) NSString *content;                          // 朋友圈新闻内容
@property (nonatomic, copy) NSArray<NSString *> *imageUrls;             // 内容中的图片地址
@property (nonatomic, copy) NSString *location;                         // 地理位置信息

@property (nonatomic, strong) NSMutableArray<PDCLike *> *likes;         // 点赞信息数组
@property (nonatomic, strong) NSNumber *likeNum;                        // 点赞数
@property (nonatomic, assign) BOOL currentUserIsLike;                   // 当前用户点赞状态

@property (nonatomic, strong) NSMutableArray<PDCComment *> *comments;   // 所有评论数组
@property (nonatomic, strong) NSNumber *commentNum;                     // 评论数

@property (nonatomic, strong) PDCUser *createUser;                      // 创建者

@property (nonatomic, strong) NSNumber *createTimestamp;                // 显示的创建时间内容

@property (nonatomic, assign) BOOL canDel;                              // 是否可以删除
@property (nonatomic, assign) BOOL showAll;                             // 折叠回复的情况 1.超过2个全展示的情况 2.小于等于2个的情况

- (BOOL)processLimitedComment:(BOOL)replace;

@end
