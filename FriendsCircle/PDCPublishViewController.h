//
//  PDCPublishViewController.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/23.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PDCPublishViewController;

@protocol PDCPublishViewControllerDelegate<NSObject>

- (void)addNewImageToCurrentMoment;
- (void)publishCurrentMomentWith:(NSString *)content andImages:(NSArray *)images;
- (void)showNearbyLocation;

@end

@interface PDCPublishViewController : UITableViewController

@property (nonatomic, weak) id<PDCPublishViewControllerDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *images;

- (instancetype)initWithImages:(NSArray *)images;

- (void)addNewImages:(NSArray *)images;

- (void)updateLocation:(NSString *)locationName;

@end
