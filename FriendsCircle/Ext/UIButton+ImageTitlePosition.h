//
//  UIButton+ImageTitlePosition.h
//  Fangxinxuan
//
//  Created by 孙燕飞 on 2017/8/21.
//  Copyright © 2017年 Koudai. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ButtonEdgeInsetsStyle) {
    // image在上，label在下
    ButtonEdgeInsetsStyleTop,
    // image在左，label在右
    ButtonEdgeInsetsStyleLeft,
    // image在下，label在上
    ButtonEdgeInsetsStyleBottom,
    // image在右，label在左
    ButtonEdgeInsetsStyleRight
};

@interface UIButton (ImageTitlePosition)

/**
 *  设置button的titleLabel和imageView的布局样式，及间距
 *
 *  @param style titleLabel和imageView的布局样式
 *  @param space titleLabel和imageView的间距
 */
- (void)layoutButtonWithEdgeInsetsStyle:(ButtonEdgeInsetsStyle)style imageTitleSpace:(CGFloat)space;

@end
