//
//  NSDate+PDC.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/3.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (PDC)

- (NSTimeInterval)timestamp;

+ (NSString *)dateStringFromTimestamp:(NSTimeInterval)timestamp withFormat:(NSString *)format;

@end
