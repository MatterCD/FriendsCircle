//
//  NSBundle+PDC.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/9.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (PDC)

+ (NSString *)pdcLocalizedStringForKey:(NSString *)key;

+ (NSString *)pdcLocalizedStringForKey:(NSString *)key value:(NSString *)value;

@end
