//
//  NSNumber+PDC.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/1.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (PDC)

- (NSString *)showTimeString;

@end
