//
//  UIScrollView+PDC.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/1.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (PDC)

- (void)scrollToTopAnimated:(BOOL)animated;

- (void)scrollToBottomAnimated:(BOOL)animated;
    
@end
