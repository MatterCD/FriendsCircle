//
//  NSNumber+PDC.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/1.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "NSNumber+PDC.h"

@implementation NSNumber (PDC)

- (NSString *)showTimeString {
    NSString *result;
    
    NSTimeInterval timestamp = [self doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp / 1000.0];
    NSDate *now = [NSDate date];
    NSTimeInterval timeInterval = [now timeIntervalSinceDate:date];
    
    long temp = 0;
    
    if (timeInterval < 60) {
        result = [NSString stringWithFormat:@"刚刚"];
    } else if((temp = timeInterval/60) < 60){
        result = [NSString stringWithFormat:@"%@分前",@(temp)];
    } else if((temp = temp/60) < 24){
        result = [NSString stringWithFormat:@"%@小时前",@(temp)];
    } else if((temp = temp/24) <30){
        result = [NSString stringWithFormat:@"%@天前",@(temp)];
    } else if((temp = temp/30) <12){
        result = [NSString stringWithFormat:@"%@月前",@(temp)];
    } else {
        temp = temp/12;
        result = [NSString stringWithFormat:@"%@年前",@(temp)];
    }
    
    return result;
}

@end
