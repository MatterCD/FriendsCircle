//
//  PDCLocalizeControl.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/9.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCLocalizeControl.h"

@implementation PDCLocalizeControl

+ (NSString *)userLanguage {
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *language = [def valueForKey:@"userLanguage"];
    return language;
}

+ (void)setUserlanguage:(NSString *)language {
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setValue:language forKey:@"userLanguage"];
    [def synchronize];
}

@end
