//
//  PDCMomentDigestView.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/4.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCMomentDigestView.h"
#import "PDCMoment.h"
#import "NSDate+PDC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Masonry/Masonry.h>

static const CGFloat MomentViewCommonSpace = 15;

@interface PDCMomentDigestView()

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong) PDCMoment *moment;

@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIImageView *momentImageView;
@property (nonatomic, strong) UILabel *momentContentLabel;
@property (nonatomic, strong) UILabel *imageNumLabel;
@property (nonatomic, strong) UIButton *delBtn;

@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@end

@implementation PDCMomentDigestView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self buildUI];
        [self layoutUI];
        [self styleUI];
    }
    return self;
}

- (void)dealloc {
    self.tapGesture = nil;
}

#pragma mark - View

- (void)buildUI {
    self.dateLabel = UILabel.new;
    [self.contentView addSubview:self.dateLabel];
    
    self.momentImageView = UIImageView.new;
    [self.contentView addSubview:self.momentImageView];
    
    self.momentContentLabel = UILabel.new;
    [self.contentView addSubview:self.momentContentLabel];
    
    self.imageNumLabel = UILabel.new;
    [self.contentView addSubview:self.imageNumLabel];
    
    self.delBtn = UIButton.new;
    [self.delBtn addTarget:self action:@selector(delete) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.delBtn];
    
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(select)];
    [self.contentView addGestureRecognizer:self.tapGesture];
}

- (void)layoutUI {
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(MomentViewCommonSpace);
        make.top.equalTo(self.contentView).offset(MomentViewCommonSpace);
        make.width.equalTo(@70);
    }];
    
    [self.momentImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(MomentViewCommonSpace * 2 + 70);
        make.top.equalTo(self.contentView).offset(MomentViewCommonSpace);
        make.width.equalTo(@100);
        make.height.equalTo(@100);
    }];
    
    [self.delBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-MomentViewCommonSpace);
        make.top.equalTo(self.contentView).offset(MomentViewCommonSpace);
        make.width.equalTo(@50);
    }];
    
    [self.imageNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.momentContentLabel);
        make.top.equalTo(self.momentContentLabel.mas_bottom).offset(MomentViewCommonSpace);
    }];
}

- (void)styleUI {
    self.dateLabel.font = [UIFont boldSystemFontOfSize:18];
    self.dateLabel.textColor = UIColorFromRGB(0x464646);
    self.dateLabel.textAlignment = NSTextAlignmentCenter;
    
    self.momentImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.momentImageView.clipsToBounds = YES;
    
    self.momentContentLabel.font = [UIFont systemFontOfSize:14];
    self.momentContentLabel.textColor = UIColorFromRGB(0x464646);
    self.momentContentLabel.numberOfLines = 2;
    
    self.imageNumLabel.font = [UIFont systemFontOfSize:12];
    self.imageNumLabel.textColor = UIColorFromRGB(0x5682B2);
    
    [self.delBtn setTitle:PDCLocalizedString(@"moment_delete") forState:UIControlStateNormal];
    [self.delBtn setTitleColor:UIColorFromRGB(0x5682B2) forState:UIControlStateNormal];
    self.delBtn.titleLabel.font = [UIFont systemFontOfSize:12];
}

- (void)bindModel:(PDCMoment *)moment withIndex:(NSInteger)index withShowDateFlag:(BOOL)showDate {
    
    self.moment = moment;
    self.index = index;
    
    if (showDate) {
        self.dateLabel.hidden = NO;
        self.dateLabel.text = [NSDate dateStringFromTimestamp:moment.createTimestamp.doubleValue withFormat:@"dd MM月"];
    } else {
        self.dateLabel.hidden = YES;
        self.dateLabel.text = @"";
    }
    
    if (moment.imageUrls.count > 0) {
        [self.momentContentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.momentImageView.mas_right).offset(MomentViewCommonSpace);
            make.top.equalTo(self.contentView).offset(MomentViewCommonSpace);
            make.right.equalTo(self.delBtn.mas_left).offset(-MomentViewCommonSpace);
        }];
        self.momentImageView.hidden = NO;
        self.imageNumLabel.hidden = NO;
        NSString *firstImageUrl = moment.imageUrls[0];
        [self.momentImageView sd_setImageWithURL:[NSURL URLWithString:firstImageUrl]];
        self.imageNumLabel.text = [NSString stringWithFormat:@"共%@张", @(moment.imageUrls.count)];
    } else {
        [self.momentContentLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(MomentViewCommonSpace * 2 + 70);
            make.top.equalTo(self.contentView).offset(MomentViewCommonSpace);
            make.right.equalTo(self.delBtn.mas_left).offset(-MomentViewCommonSpace);
        }];
        self.momentImageView.hidden = YES;
        self.imageNumLabel.hidden = YES;
    }
    self.momentContentLabel.text = moment.content;
}

+ (CGFloat)heightForModel:(PDCMoment *)moment {
    if (moment.imageUrls.count > 0) {
        return 100 + MomentViewCommonSpace * 2;
    } else {
        return 50;
    }
}

#pragma mark - Control

- (void)select {
    if (self.delegate && [self.delegate respondsToSelector:@selector(digestMomentView:didSelectMoment:atIndex:)]) {
        [self.delegate digestMomentView:self didSelectMoment:self.moment atIndex:self.index];
    }
}

- (void)delete {
    if (self.delegate && [self.delegate respondsToSelector:@selector(digestMomentView:doDeleteMoment:)]) {
        [self.delegate digestMomentView:self doDeleteMoment:self.moment];
    }
}

@end
