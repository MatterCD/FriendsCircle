//
//  PDCSudokuBoxView.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/10/16.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCSudokuBoxView.h"
#import <SDWebImage/UIImageView+WebCache.h>

static const NSInteger DefaultColumnNumber = 3;
static const CGFloat SudokuImageCommonSpace = 5;

@interface PDCSudokuBoxView()

@end

@implementation PDCSudokuBoxView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self resetImageContainer];
        _columnNumber = DefaultColumnNumber;
    }
    return self;
}

- (void)dealloc {
    
}

- (void)resetImageContainer {
    for (UIImageView *imageView in self.imageViewItems) {
        [imageView removeFromSuperview];
    }
    _imageViewItems = [NSMutableArray array];
}

- (void)setItemsSource:(NSArray *)itemsSource {
    _itemsSource = itemsSource;
    [self resetImageContainer];
    for (NSUInteger index = 0; index < _itemsSource.count; index++) {
        UIImageView *imageView = [UIImageView new];
        imageView.backgroundColor = [UIColor grayColor];
        if ([_itemsSource[index] isKindOfClass:[UIImage class]]) {
            imageView.image = _itemsSource[index];
        } else if ([_itemsSource[index] isKindOfClass:[NSString class]]) {
            NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", _itemsSource[index]]];
            [imageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeHolder"]];
        } else if ([_itemsSource isKindOfClass:[NSURL class]]) {
            [imageView sd_setImageWithURL:_itemsSource[index] placeholderImage:[UIImage imageNamed:@"placeHolder"]];
        }
        imageView.userInteractionEnabled = YES;
        imageView.tag = index;
        imageView.clipsToBounds = YES;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:imageView];
        [_imageViewItems addObject:imageView];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImageAction:)];
        [imageView addGestureRecognizer:tapGesture];
    }
}

- (void)layoutSubviews {
    CGFloat width = (self.frame.size.width - SudokuImageCommonSpace * 2) / self.columnNumber;
    CGFloat height = width;
    for (NSUInteger index = 0; index < self.imageViewItems.count; index ++) {
        UIImageView *imageView = self.imageViewItems[index];
        CGFloat x =  (width + SudokuImageCommonSpace) * (index % self.columnNumber);
        CGFloat y = floorf(index / (CGFloat)self.columnNumber) * (height + SudokuImageCommonSpace);
        imageView.frame = CGRectMake(x, y, width, height);
    }
}

#pragma mark - Control

- (void)tapImageAction:(UITapGestureRecognizer *)tap {
    UIImageView *tappedImageView = (UIImageView *)tap.view;
    NSInteger imageIndex = tappedImageView.tag;
    if (self.delegate && [self.delegate respondsToSelector:@selector(sudoKuBoxView:didTapImageViewAtIndex:)]) {
        [self.delegate sudoKuBoxView:self didTapImageViewAtIndex:imageIndex];
    }
}

#pragma mark - Util

+ (CGFloat)heightForItemsSource:(NSArray *)itemsSource width:(CGFloat)width {
    return [self heightForItemsSource:itemsSource width:width andColumnNumber:DefaultColumnNumber];
}

+ (CGFloat)heightForItemsSource:(NSArray *)itemsSource width:(CGFloat)width andColumnNumber:(NSInteger)columnNumber {
    NSUInteger lineNumber = ceilf(itemsSource.count / (CGFloat)columnNumber);
    CGFloat imageWidth = (width - SudokuImageCommonSpace * 2) / columnNumber;
    CGFloat imageHeight = imageWidth;
    return lineNumber * (imageHeight + SudokuImageCommonSpace) - SudokuImageCommonSpace;
}

@end
