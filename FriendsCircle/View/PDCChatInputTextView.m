//
//  PDCChatInputTextView.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/16.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCChatInputTextView.h"

#define IsTextContainFace(text) [text containsString:@"["] &&  [text containsString:@"]"] && [[text substringFromIndex:text.length - 1] isEqualToString:@"]"]

@implementation PDCChatInputTextView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.font = [UIFont systemFontOfSize:14.f];
        self.textColor = [UIColor blackColor];
        self.layer.borderColor = UIColorFromRGB(0xCCCCCC).CGColor;
        self.layer.cornerRadius = 2.0f;
        self.layer.borderWidth = 1.0f;
        self.contentMode = UIViewContentModeRedraw;
        self.dataDetectorTypes = UIDataDetectorTypeNone;
        self.returnKeyType = UIReturnKeySend;
        self.enablesReturnKeyAutomatically = YES;
        
        UIEdgeInsets textContainerInset = self.textContainerInset;
        textContainerInset.left = 10;
        textContainerInset.right = 10;
        self.textContainerInset = textContainerInset;
        
        _placeHolder = nil;
        _placeHolderTextColor = UIColorFromRGB(0xCCCCCC);
        
        [self _addTextViewNotificationObservers];
    }
    return self;
}

- (void)dealloc {
    [self _removeTextViewNotificationObservers];
}

- (void)deleteBackward {
    if (IsTextContainFace(self.text)) { // 如果text中有表情
        if ([self.inputTextViewDelegate respondsToSelector:@selector(textViewDeleteBackward:)]) {
            [self.inputTextViewDelegate textViewDeleteBackward:self];
        }
    }else {
        [super deleteBackward];
    }
}

#pragma mark - FXXTextView 方法

- (NSUInteger)numberOfLinesOfText {
    return [PDCChatInputTextView numberOfLinesForMessage:self.text];
}

+ (NSUInteger)maxCharactersPerLine {
    return ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)? 33:109;
}

+ (NSUInteger)numberOfLinesForMessage:(NSString *)text {
    return (text.length / [PDCChatInputTextView maxCharactersPerLine]) + 1;
}

#pragma mark -- Setters
- (void)setPlaceHolder:(NSString *)placeHolder {
    if ([placeHolder isEqualToString:_placeHolder]) {
        return;
    }
    _placeHolder = [placeHolder copy];
    [self setNeedsDisplay];
}

- (void)setPlaceHolderTextColor:(UIColor *)placeHolderTextColor {
    if ([placeHolderTextColor isEqual:_placeHolderTextColor]) {
        return;
    }
    _placeHolderTextColor = placeHolderTextColor;
    [self setNeedsDisplay];
}

#pragma mark -- UITextView overrides

- (void)setText:(NSString *)text {
    [super setText:text];
    [self setNeedsDisplay];
}

- (void)setAttributedText:(NSAttributedString *)attributedText {
    [super setAttributedText:attributedText];
    [self setNeedsDisplay];
}

- (void)setFont:(UIFont *)font {
    [super setFont:font];
    [self setNeedsDisplay];
}

- (void)setTextAlignment:(NSTextAlignment)textAlignment {
    [super setTextAlignment:textAlignment];
    [self setNeedsDisplay];
}

#pragma mark -- Drawing

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    if ([self.text length] == 0 && self.placeHolder) {
        [self.placeHolderTextColor set];
        [self.placeHolder drawInRect:CGRectInset(rect, 15.0f, 7.5f) withAttributes:[self _placeholderTextAttributes]];
    }
}

#pragma mark -- Notifications

- (void)_addTextViewNotificationObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_didReceiveTextViewNotification:)
                                                 name:UITextViewTextDidChangeNotification
                                               object:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_didReceiveTextViewNotification:)
                                                 name:UITextViewTextDidBeginEditingNotification
                                               object:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(_didReceiveTextViewNotification:)
                                                 name:UITextViewTextDidEndEditingNotification
                                               object:self];
}

- (void)_didReceiveTextViewNotification:(NSNotification *)notification {
    [self setNeedsDisplay];
}

- (void)_removeTextViewNotificationObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextViewTextDidChangeNotification
                                                  object:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextViewTextDidBeginEditingNotification
                                                  object:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextViewTextDidEndEditingNotification
                                                  object:self];
}

- (NSDictionary *)_placeholderTextAttributes {
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    paragraphStyle.alignment = self.textAlignment;
    
    return @{ NSFontAttributeName : self.font,
              NSForegroundColorAttributeName : self.placeHolderTextColor,
              NSParagraphStyleAttributeName : paragraphStyle };
}

@end
