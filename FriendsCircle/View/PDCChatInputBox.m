//
//  ChatInputBox.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/15.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCChatInputBox.h"
#import "PDCChatInputTextView.h"

#define TextViewH 32

@interface PDCChatInputBox ()<UITextViewDelegate>

@property (nonatomic, strong) PDCChatInputTextView *textView;

@end

@implementation PDCChatInputBox

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"self.textView.contentSize"];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColorFromRGB(0xF6F6F6);
        self.previousTextViewHeight = TextViewH;
        [self initSubview];
    }
    return self;
}

- (void)initSubview {
    self.textView = [PDCChatInputTextView new];
    self.textView.frame = CGRectMake(20, (self.frame.size.height - 32) / 2, self.frame.size.width - 40, TextViewH);
    self.textView.delegate = self;
    
    self.textView.layoutManager.allowsNonContiguousLayout = NO;

    [self addSubview:self.textView];
    [self addObserver:self forKeyPath:@"self.textView.contentSize" options:(NSKeyValueObservingOptionNew) context:nil];
}

- (BOOL)isFirstUp {
    return self.textView.isFirstResponder;
}

- (void)clearText {
    
    self.textView.text = nil;
}

- (void)keyboardUp {
    [self.textView becomeFirstResponder];
}

- (void)keyboardDown {
    [self.textView resignFirstResponder];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        if ([self.delegate respondsToSelector:@selector(chatInputBoxSendText:)])
        {
            self.currentText = @"";
            [self.delegate chatInputBoxSendText:textView.text];
            textView.text = @"";
        }
        return NO;
    }
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if ([self.delegate respondsToSelector:@selector(chatInputBoxShouldBeginEditing)]) {
        return [self.delegate chatInputBoxShouldBeginEditing];
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatInputBoxBeginEditing)]) {
        [self.delegate chatInputBoxBeginEditing];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    if (self.delegate && [self.delegate respondsToSelector:@selector(chatInputBoxEndEditing)]) {
        [self.delegate chatInputBoxEndEditing];
    }
}

#pragma mark -- 调整文本内容

- (void)setTextViewContent:(NSString *)text {
    self.currentText = self.textView.text = text;
}

- (void)clearTextViewContent {
    self.currentText = self.textView.text = @"";
}

#pragma mark -- 调整placeHolder

- (void)setTextViewPlaceHolder:(NSString *)placeholder {
    if (placeholder == nil) {
        return;
    }
    self.textView.placeHolder = placeholder;
}

- (void)setTextViewPlaceHolderColor:(UIColor *)placeHolderColor {
    if (placeHolderColor == nil) {
        return;
    }
    self.textView.placeHolderTextColor = placeHolderColor;
}

#pragma mark - kvo回调

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if (object == self && [keyPath isEqualToString:@"self.textView.contentSize"]) {
        [self layoutAndAnimateTextView:self.textView];
    }
}

#pragma mark -- 计算textViewContentSize改变

- (CGFloat)getTextViewContentH:(PDCChatInputTextView *)textView {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        return ceilf([textView sizeThatFits:textView.frame.size].height);
    } else {
        return textView.contentSize.height;
    }
}

- (CGFloat)fontHeight {
    return self.textView.font.lineHeight; //14号字体
}

- (CGFloat)maxLines {
    CGFloat h = [[UIScreen mainScreen] bounds].size.height;
    CGFloat line = 5;
    if (h == 480) {
        line = 3;
    }else if (h == 568){
        line = 4;
    }else if (h == 667){
        line = 6;
    }else if (h == 736){
        line = 6;
    }
    return line;
}

- (void)layoutAndAnimateTextView:(PDCChatInputTextView *)textView {
    CGFloat maxHeight = [self fontHeight] * [self maxLines];
    CGFloat contentH = [self getTextViewContentH:textView];
    
    BOOL isShrinking = contentH < self.previousTextViewHeight;
    CGFloat changeInHeight = contentH - self.previousTextViewHeight;
    
    if (!isShrinking && (self.previousTextViewHeight == maxHeight || textView.text.length == 0)) {
        changeInHeight = 0;
    } else {
        changeInHeight = MIN(changeInHeight, maxHeight - self.previousTextViewHeight);
    }
    
    if (changeInHeight != 0.0f) {
        [UIView animateWithDuration:0.25f animations:^{
            if (isShrinking) {
                if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
                    self.previousTextViewHeight = MIN(contentH, maxHeight);
                }
                // if shrinking the view, animate text view frame BEFORE input view frame
                [self adjustTextViewHeightBy:changeInHeight];
            }
            CGRect inputViewFrame = self.frame;
            self.frame = CGRectMake(0.0f, inputViewFrame.origin.y - changeInHeight, inputViewFrame.size.width, (inputViewFrame.size.height + changeInHeight));
            if (!isShrinking) {
                if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0) {
                    self.previousTextViewHeight = MIN(contentH, maxHeight);
                }
                // growing the view, animate the text view frame AFTER input view frame
                [self adjustTextViewHeightBy:changeInHeight];
            }
        } completion:^(BOOL finished) {
        }];
        self.previousTextViewHeight = MIN(contentH, maxHeight);
    }
    
    // Once we reached the max height, we have to consider the bottom offset for the text view.
    // To make visible the last line, again we have to set the content offset.
    if (self.previousTextViewHeight == maxHeight) {
        double delayInSeconds = 0.01;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime,
                       dispatch_get_main_queue(), ^(void) {
                           CGPoint bottomOffset = CGPointMake(0.0f, contentH - textView.bounds.size.height);
                           [textView setContentOffset:bottomOffset animated:YES];
                       });
    }
}

- (void)adjustTextViewHeightBy:(CGFloat)changeInHeight {
    //动态改变自身的高度和输入框的高度
    CGRect prevFrame = self.textView.frame;
    
    NSUInteger numLines = MAX([self.textView numberOfLinesOfText],
                              [[self.textView.text componentsSeparatedByString:@"\n"] count] + 1);
    
    
    self.textView.frame = CGRectMake(prevFrame.origin.x, prevFrame.origin.y, prevFrame.size.width, prevFrame.size.height + changeInHeight);
    
    self.textView.contentInset = UIEdgeInsetsMake((numLines >=6 ? 4.0f : 0.0f), 0.0f, (numLines >=6 ? 4.0f : 0.0f), 0.0f);
    // from iOS 7, the content size will be accurate only if the scrolling is enabled.
    //self.messageInputTextView.scrollEnabled = YES;
    if (numLines >=6) {
        CGPoint bottomOffset = CGPointMake(0.0f, self.textView.contentSize.height-self.textView.bounds.size.height);
        [self.textView setContentOffset:bottomOffset animated:YES];
        [self.textView scrollRangeToVisible:NSMakeRange(self.textView.text.length-2, 1)];
    }
}

@end
