//
//  PDCMomentHeaderView.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/29.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCMomentHeaderView.h"
#import "UIView+PDC.h"
#import "UIImage+PDC.h"
#import <Masonry/Masonry.h>

@interface PDCMomentHeaderView()

@end

@implementation PDCMomentHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self buildUI];
    }
    return self;
}

- (void)buildUI {
    self.backgroundColor = [UIColor whiteColor];
    typeof(self) __weak weakSelf = self;
    
    _backgroundImageView = [UIImageView new];
    _backgroundImageView.image = [UIImage pdcImageNamed:@"AlbumCover"];
    _backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    _backgroundImageView.clipsToBounds = YES;
    _backgroundImageView.userInteractionEnabled = YES;
    [_backgroundImageView setTapActionWithBlock:^{
        if (weakSelf.backgroundImageViewClick) {
            weakSelf.backgroundImageViewClick();
        }
    }];

    _iconView = [UIImageView new];
    _iconView.userInteractionEnabled = YES;
    _iconView.image = [UIImage pdcImageNamed:@"user_default"];
    _iconView.layer.borderColor = [UIColor whiteColor].CGColor;
    _iconView.layer.borderWidth = 3;
    [_iconView setTapActionWithBlock:^{
        if (weakSelf.iconButtonClick) {
            weakSelf.iconButtonClick();
        }
    }];
    
    _nameLabel = [UILabel new];
    _nameLabel.textColor = [UIColor whiteColor];
    _nameLabel.textAlignment = NSTextAlignmentRight;
    _nameLabel.font = [UIFont boldSystemFontOfSize:16];
    
    [self addSubview:_backgroundImageView];
    [self addSubview:_iconView];
    [self addSubview:_nameLabel];
    
    [_backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).insets(UIEdgeInsetsMake(0, 0, 20, 0));
    }];
    
    [_iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(70, 70));
        make.right.equalTo(self.mas_right).offset(-15);
        make.bottom.equalTo(self);
    }];
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_iconView.mas_left).offset(-15);
        make.centerY.equalTo(_iconView);
    }];
}

- (void)updateHeight:(CGFloat)height {
    self.height = height;
    if (self.height == 200) {
        _backgroundImageView.contentMode = UIViewContentModeScaleToFill;
    } else {
        _backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    [self layoutIfNeeded];
}

@end
