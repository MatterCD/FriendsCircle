//
//  PDCMomentDigestView.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/4.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PDCMoment;
@class PDCMomentDigestView;

@protocol PDCMomentDigestViewDelegate<NSObject>

- (void)digestMomentView:(PDCMomentDigestView *)momentDigestView didSelectMoment:(PDCMoment *)moment atIndex:(NSInteger)index;
- (void)digestMomentView:(PDCMomentDigestView *)momentDigestView doDeleteMoment:(PDCMoment *)moment;

@end

@interface PDCMomentDigestView : UITableViewHeaderFooterView

@property (nonatomic, weak) id<PDCMomentDigestViewDelegate> delegate;

- (void)bindModel:(PDCMoment *)moment withIndex:(NSInteger)index withShowDateFlag:(BOOL)showDate;

+ (CGFloat)heightForModel:(PDCMoment *)moment;

@end
