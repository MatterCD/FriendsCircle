//
//  PDCSudokuBoxView.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/10/16.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PDCSudokuBoxView;

@protocol PDCSudokuBoxViewDelegate <NSObject>

- (void)sudoKuBoxView:(PDCSudokuBoxView *)boxView didTapImageViewAtIndex:(NSInteger)imageIndex;

@end

@interface PDCSudokuBoxView : UIView

@property (nonatomic, assign) NSInteger columnNumber;   // default is 3
@property (nonatomic, copy) NSArray *itemsSource;
@property (nonatomic, strong, readonly) NSMutableArray *imageViewItems;
@property (nonatomic, weak) id<PDCSudokuBoxViewDelegate> delegate;

+ (CGFloat)heightForItemsSource:(NSArray *)itemsSource width:(CGFloat)width;
+ (CGFloat)heightForItemsSource:(NSArray *)itemsSource width:(CGFloat)width andColumnNumber:(NSInteger)columnNumber;

@end
