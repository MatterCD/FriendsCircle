//
//  PDCMomentHeaderView.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/29.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDCMomentHeaderView : UIView

@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UIImageView *iconView;
@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, copy) void (^iconButtonClick)(void);
@property (nonatomic, copy) void (^backgroundImageViewClick)(void);

- (void)updateHeight:(CGFloat)height;

@end
