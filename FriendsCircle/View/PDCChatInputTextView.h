//
//  PDCChatInputTextView.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/16.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PDCChatInputTextView;

@protocol PDCChatInputTextViewDelegate <UITextViewDelegate>

- (void)textViewDeleteBackward:(PDCChatInputTextView *)textView;

@end

@interface PDCChatInputTextView : UITextView

@property (nonatomic, weak) id<PDCChatInputTextViewDelegate> inputTextViewDelegate;
@property (nonatomic, copy) NSString *placeHolder;
@property (nonatomic, strong) UIColor *placeHolderTextColor;

- (NSUInteger)numberOfLinesOfText;

@end
