//
//  PDCOperationMenu.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/11.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCOperationMenu.h"
#import "UIImage+PDC.h"
#import <Masonry/Masonry.h>

@implementation PDCOperationMenu {
    UIButton *_likeButton;
    UIButton *_commentButton;
    UIButton *_favouritesButton;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (void)setup {
    self.clipsToBounds = YES;
    self.layer.cornerRadius = 5;
    self.backgroundColor = UIColorFromRGB(0x454A4C);
    
    _likeButton = [self creatButtonWithTitle:PDCLocalizedString(@"moment_like") image:[UIImage pdcImageNamed:@"AlbumLike"] selImage:[UIImage imageNamed:@""] target:self selector:@selector(likeButtonClicked)];
    _commentButton = [self creatButtonWithTitle:PDCLocalizedString(@"moment_comment") image:[UIImage pdcImageNamed:@"AlbumComment"] selImage:[UIImage imageNamed:@""] target:self selector:@selector(commentButtonClicked)];
    
    NSUserDefaults *userfaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    NSString *locLang = [languages objectAtIndex:0]; // 当前语言
    if ([userfaults objectForKey:@"myLanguage"]==nil) {
        if ([locLang isEqualToString:@"zh-Hans-CN"]) {
            _favouritesButton = [self creatButtonWithTitle:@"收藏" image:[UIImage pdcImageNamed:@"AlbumLike"] selImage:[UIImage imageNamed:@""] target:self selector:@selector(favouritesButtonClicked)];
        }else{
            _favouritesButton = [self creatButtonWithTitle:@"Favourite" image:[UIImage pdcImageNamed:@"AlbumLike"] selImage:[UIImage imageNamed:@""] target:self selector:@selector(favouritesButtonClicked)];
        }
    }else{
        if ([[userfaults objectForKey:@"myLanguage"] isEqual:@"zh-Hans"]) {
            _favouritesButton = [self creatButtonWithTitle:@"收藏" image:[UIImage pdcImageNamed:@"AlbumLike"] selImage:[UIImage imageNamed:@""] target:self selector:@selector(favouritesButtonClicked)];
        }else{
            _favouritesButton = [self creatButtonWithTitle:@"Favourite" image:[UIImage pdcImageNamed:@"AlbumLike"] selImage:[UIImage imageNamed:@""] target:self selector:@selector(favouritesButtonClicked)];
        }
    }
    
    
    UIView *centerLine = [UIView new];
    centerLine.backgroundColor = [UIColor grayColor];
    
    UIView *centerLine1 = [UIView new];
    centerLine1.backgroundColor = [UIColor grayColor];
    
    [self addSubview:_likeButton];
    [self addSubview:_commentButton];
    [self addSubview:_favouritesButton];
    [self addSubview:centerLine];
    [self addSubview:centerLine1];
    
    CGFloat margin = 5;
    
    [_likeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(margin);
        make.top.equalTo(self);
        make.bottom.equalTo(self);
        make.width.equalTo(@70);
    }];
    
    [centerLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_likeButton.mas_right).offset(margin);
        make.top.equalTo(self).offset(margin);
        make.bottom.equalTo(self).offset(-margin);
        make.width.equalTo(@1);
    }];
    
    [_commentButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(centerLine.mas_right).offset(margin);
        make.top.equalTo(_likeButton);
        make.bottom.equalTo(_likeButton);
        make.width.equalTo(_likeButton);
    }];
    
    [centerLine1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_commentButton.mas_right).offset(margin);
        make.top.equalTo(self).offset(margin);
        make.bottom.equalTo(self).offset(-margin);
        make.width.equalTo(@1);
    }];
    
    [_favouritesButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(centerLine1.mas_right).offset(margin);
        make.top.equalTo(_likeButton);
        make.bottom.equalTo(_likeButton);
        make.width.equalTo(_likeButton);
    }];
}

- (UIButton *)creatButtonWithTitle:(NSString *)title image:(UIImage *)image selImage:(UIImage *)selImage target:(id)target selector:(SEL)sel {
    UIButton *btn = [UIButton new];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setImage:image forState:UIControlStateNormal];
    [btn setImage:selImage forState:UIControlStateSelected];
    [btn addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    btn.titleLabel.font = [UIFont systemFontOfSize:10];
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 3, 0, 0);
    return btn;
}

- (void)likeButtonClicked {
    if (self.likeButtonClickedOperation) {
        self.likeButtonClickedOperation();
    }
    self.show = NO;
}

- (void)commentButtonClicked {
    if (self.commentButtonClickedOperation) {
        self.commentButtonClickedOperation();
    }
    self.show = NO;
}

- (void)favouritesButtonClicked {
    if (self.favouritesButtonClickedOperation) {
        self.favouritesButtonClickedOperation();
    }
    self.show = NO;
}
@end
