//
//  PDCLocalizeControl.h
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/9.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDCLocalizeControl : NSObject

+ (void)setUserlanguage:(NSString *)language;

+ (NSString *)userLanguage;


@end
