//
//  PDCMomentDetailViewController.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/12/4.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCMomentDetailViewController.h"
#import "PDCMoment.h"
#import "PDCMomentView.h"
#import "YYPhotoGroupView.h"
#import "UIView+PDC.h"
#import "PDCSudokuBoxView.h"

@interface PDCMomentDetailViewController ()<PDCMomentViewDelegate>

@property (nonatomic, strong) PDCMomentView *momentView;

@end

@implementation PDCMomentDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self buildUI];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - View

- (void)buildUI {
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = PDCLocalizedString(@"post_detail");
    self.momentView = [PDCMomentView new];
    self.momentView.delegate = self;
    CGFloat height = [PDCMomentView heightForModel:self.moment];
    self.momentView.frame = CGRectMake(0, 0, self.view.width, height);
    [self.view addSubview:self.momentView];
}

#pragma mark - PDCMomentViewDelegate

- (void)momentView:(PDCMomentView *)momentView doTapBoxView:(PDCSudokuBoxView *)boxView withMoment:(PDCMoment *)moment andImageIndex:(NSInteger)imageIndex {
    UIView *fromView = nil;
    NSMutableArray *items = [NSMutableArray new];
    for (NSUInteger i = 0; i < moment.imageUrls.count; i++) {
        UIImageView *imgView = boxView.imageViewItems[i];
        NSString *imageUrl = moment.imageUrls[i];
        
        YYPhotoGroupItem *item = [YYPhotoGroupItem new];
        item.thumbView = imgView;
        item.largeImageURL = [NSURL URLWithString:imageUrl];
        [items addObject:item];
        if (i == imageIndex) {
            fromView = imgView;
        }
    }
    YYPhotoGroupView *photoGroupView = [[YYPhotoGroupView alloc] initWithGroupItems:items];
    photoGroupView.blurEffectBackground = NO;
    [photoGroupView presentFromImageView:fromView toContainer:self.navigationController.view animated:YES completion:nil];
}

#pragma mark -
#pragma mark - Data

- (void)initData {
    
}

- (void)loadData {
    [self.momentView bindModel:self.moment withIndex:-1];
}

#pragma mark -
#pragma mark - Control

@end
