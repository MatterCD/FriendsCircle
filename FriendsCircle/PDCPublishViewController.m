//
//  PDCPublishViewController.m
//  FriendsCircle
//
//  Created by 孙燕飞 on 2017/11/23.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PDCPublishViewController.h"
#import "PDCSudokuBoxView.h"
#import "PlaceholderTextView.h"
#import "PDCPublishPlainCell.h"
#import "PDCPublishPictureCell.h"
#import "UITableViewCell+PDC.h"
#import "UIImage+PDC.h"
#import <Masonry/Masonry.h>

@interface PDCPublishViewController ()<PDCPublishPictureCellDelegate, UITextViewDelegate>

@property (nonatomic, strong) PlaceholderTextView *textView;

@end

@implementation PDCPublishViewController

- (instancetype)initWithImages:(NSArray *)images {
    if (self = [super init]) {
        _images = [NSMutableArray array];
        if (images != nil) {
            [_images addObjectsFromArray:images];
        }
        [_images addObject:[UIImage pdcImageNamed:@"AlbumAddBtn"]];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self buildUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark -
#pragma mark - View

- (void)buildUI {
    [self buildNavigationUI];
    [self buildContentUI];
}

- (void)buildNavigationUI {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:PDCLocalizedString(@"post") style:UIBarButtonItemStyleDone target:self action:@selector(send)];
    self.navigationItem.rightBarButtonItem.enabled = NO;
}

- (void)buildContentUI {
    
    self.textView = [PlaceholderTextView new];
    self.textView.font = [UIFont systemFontOfSize:16];
    self.textView.placeholder = PDCLocalizedString(@"post_placeholder");
    self.textView.scrollEnabled = YES;
    self.textView.delegate = self;
    self.textView.bounces = YES;
    self.textView.editable = YES;
    self.textView.selectable = YES;
    self.textView.showsHorizontalScrollIndicator = NO;
    self.textView.showsVerticalScrollIndicator = YES;
    self.textView.delegate = self;
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 100)];
    [headerView addSubview:self.textView];
    self.tableView.tableHeaderView = headerView;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self.textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(headerView).insets(UIEdgeInsetsMake(5, 15, 5, 15));
    }];
}

#pragma mark - UTTextView Delegate

- (void)textViewDidChange:(UITextView *)textView {
    self.navigationItem.rightBarButtonItem.enabled = (textView.text.length != 0);
}

#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0){
        PDCPublishPictureCell *cell = [PDCPublishPictureCell cellWithTableView:tableView];
        cell.delegate = self;
        [cell bindModel:self.images];
        return cell;
    } else {
        PDCPublishPlainCell *cell = [PDCPublishPlainCell cellWithTableView:tableView];
        cell.textLabel.text = PDCLocalizedString(@"location");
        cell.textLabel.font = [UIFont systemFontOfSize:16];
        cell.imageView.image = [UIImage pdcImageNamed:@"AlbumLocationIcon"];
        return cell;
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        return [PDCPublishPictureCell estimatedHeightForModel:self.images];
    }
    return 40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 1 && [self.delegate respondsToSelector:@selector(showNearbyLocation)]) {
        [self.delegate showNearbyLocation];
    }
}

#pragma mark - PDCPublishPictureCellDelegate

- (void)didSelectAddItem {
    if (self.delegate && [self.delegate respondsToSelector:@selector(addNewImageToCurrentMoment)]) {
        [self.delegate addNewImageToCurrentMoment];
    }
}

#pragma mark -
#pragma mark - Data

- (void)addNewImages:(NSArray *)images {
    UIImage *lastObj = self.images.lastObject;
    [self.images removeLastObject];
    [self.images addObjectsFromArray:images];
    if (self.images.count < 9) {
        [self.images addObject:lastObj];
    }
    [self.tableView reloadData];
}

- (void)updateLocation:(NSString *)locationName {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    UITableViewCell *locationCell = [self.tableView cellForRowAtIndexPath:indexPath];
    locationCell.textLabel.text = locationName;
}

#pragma mark -
#pragma mark - Controller

- (void)send {
    NSString *content = self.textView.text;
    NSMutableArray *images = self.images.mutableCopy;
    if (images.count < 9) {
         [images removeLastObject];
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(publishCurrentMomentWith:andImages:)]) {
        [self.delegate publishCurrentMomentWith:content andImages:images];
    }
}

@end
