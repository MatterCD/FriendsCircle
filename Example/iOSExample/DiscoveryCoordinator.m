//
//  DiscoveryCoordinator.m
//  iOSExample
//
//  Created by 孙燕飞 on 2017/11/28.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "DiscoveryCoordinator.h"
#import "TZImagePickerController.h"
#import "PublishCoordinator.h"

typedef NS_ENUM(NSInteger, PickMediaDataMode) {
    ForNewMoment,
    ForNewCover
};

static const CGFloat FakeDataRequestDuration = 1.0;

@interface DiscoveryCoordinator()<PDCMomentsViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UINavigationController *navigationController;
@property (nonatomic, strong) PDCMomentsViewController *momentsViewController;
@property (nonatomic, strong) PublishCoordinator *publishCoordinator;
@property (nonatomic, strong) DiscoveryCoordinator *mineOwnTimelineCoordinator;

@property (nonatomic, assign) PickMediaDataMode pickMediaDataMode;

@end

@implementation DiscoveryCoordinator

- (instancetype)initWithMomentsViewController:(PDCMomentsViewController *)momentsViewController {
    self = [super init];
    if (self) {
        self.momentsViewController = momentsViewController;
        self.momentsViewController.delegate = self;
        self.navigationController = momentsViewController.navigationController;
    }
    return self;
}

#pragma mark -
#pragma mark - PDCMomentsViewController Delegate

- (BOOL)isUserLoggedIn {
    return YES;
}

- (void)dataLoadMoments:(BOOL)isMore {
    if (!isMore) {
        [self loadMomentsData];
    } else {
        [self loadMoreMomentsData];
    }
}

- (void)actionAddNewMoment {
    self.pickMediaDataMode = ForNewMoment;
    UIAlertController *chooseMediaController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [chooseMediaController addAction:[UIAlertAction actionWithTitle:@"拍摄" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showSystemCameraPicker];
    }]];
    [chooseMediaController addAction:[UIAlertAction actionWithTitle:@"从手机相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showPhotoSelectPicker];
    }]];
    [chooseMediaController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDestructive handler:nil]];
    [self.navigationController presentViewController:chooseMediaController animated:YES completion:nil];
}

- (void)actionUserAvatarClick {
    [self navigateToMineTimeline];
}

- (void)actionHeaderImageViewClick {
    self.pickMediaDataMode = ForNewCover;
    UIAlertController *chooseMediaController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [chooseMediaController addAction:[UIAlertAction actionWithTitle:@"拍摄" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showSystemCameraPicker];
    }]];
    [chooseMediaController addAction:[UIAlertAction actionWithTitle:@"从手机相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self showPhotoSelectPicker];
    }]];
    [chooseMediaController addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDestructive handler:nil]];
    [self.navigationController presentViewController:chooseMediaController animated:YES completion:nil];
}

- (void)actionLikeMoment:(PDCMoment *)moment {
    NSLog(@"actionLikeMoment ....");
    [self.momentsViewController updateMoment:moment likeStatus:!moment.currentUserIsLike];
}

- (void)actionCommentMoment:(PDCMoment *)moment withContent:(NSString *)content {
    NSLog(@"actionCommentMoment .... content is :%@", content);
}

- (void)actionDeleteMoment:(PDCMoment *)moment {
    NSLog(@"actionDeleteMoment ....");
}

- (void)actionClickUserAvatarOfMoment:(PDCMoment *)moment {
    [self navigateToMineTimeline];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        if ([mediaType isEqualToString:@"public.image"]) {
            switch (self.pickMediaDataMode) {
                case ForNewMoment:
                {
                    UIImage *cameraCaptureImage = [info objectForKey:UIImagePickerControllerOriginalImage];
                    [self navigateToPublishViewController:@[cameraCaptureImage]];
                }
                    break;
                case ForNewCover:
                {
                    
                }
                default:
                    break;
            }
        }
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark - Data

- (void)loadMomentsData {
    switch (self.momentsViewController.showMode) {
        case AllFriends:
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(FakeDataRequestDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.momentsViewController updateUserNameView:@"AFI"];
                NSArray<PDCMoment *> *resultMoments = [self fakeDataWithJsonFile:@"fake_friends_circle_list_complex"];
                [self.momentsViewController addMoments:resultMoments withRealodFlag:YES];
            });
        }
            break;
        case MineOwn:
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(FakeDataRequestDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.momentsViewController updateUserNameView:@"AFI"];
                NSArray<PDCMoment *> *resultMoments = [self fakeDataWithJsonFile:@"fake_friends_circle_list"];
                [self.momentsViewController addMoments:resultMoments withRealodFlag:YES];
            });
        }
            break;
        default:
            break;
    }
}

- (void)loadMoreMomentsData {
    switch (self.momentsViewController.showMode) {
        case AllFriends:
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(FakeDataRequestDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSArray<PDCMoment *> *resultMoments = [self fakeDataWithJsonFile:@"fake_friends_circle_list_with_comments"];
                [self.momentsViewController addMoments:resultMoments withRealodFlag:NO];
            });
        }
            break;
        case MineOwn:
        {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(FakeDataRequestDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSArray<PDCMoment *> *resultMoments = [self fakeDataWithJsonFile:@"fake_friends_circle_list_complex"];
                [self.momentsViewController addMoments:resultMoments withRealodFlag:NO];
            });
        }
            break;
        default:
            break;
    }
}

#pragma mark - Data Fake

- (NSArray<PDCMoment *> *)fakeDataWithJsonFile:(NSString *)fileName {
    NSString *filePath                         = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    NSString *jsonContent                      = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSDictionary *fakeData                     = [NSJSONSerialization JSONObjectWithData:[jsonContent dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil];
    NSArray *resultArray                       = fakeData[@"result"];
    NSMutableArray<PDCMoment *> *resultMoments = [NSMutableArray<PDCMoment *> new];
    
    for (NSDictionary *momentItemDict in resultArray) {
        NSDictionary *momentinfoDict = momentItemDict[@"discuss"];
        PDCMoment *moment            = [PDCMoment new];
        moment.momentId              = momentinfoDict[@"discuss_id"];
        moment.content               = momentinfoDict[@"content"];
        moment.imageUrls             = momentinfoDict[@"image_urls"];
        moment.currentUserIsLike     = [momentinfoDict[@"is_liked"] boolValue];
        moment.likeNum               = momentinfoDict[@"liked_num"];
        moment.commentNum            = momentinfoDict[@"comment_num"];
        moment.createTimestamp       = momentinfoDict[@"create_time"];
        moment.canDel                = [momentinfoDict[@"can_delete"] boolValue];
        
        NSMutableArray *likes = [NSMutableArray array];
        for (NSInteger index = 0; index < 10; index ++) {
            PDCUser *createdUser = [PDCUser new];
            createdUser.userId = @"u_id";
            createdUser.nickName = @"AFIAFIAFI";
            PDCLike *like = [PDCLike new];
            like.createdUser = createdUser;
            [likes addObject:like];
        }
        moment.likes = likes;
        
        NSArray *commentsArray        = momentItemDict[@"comments"];
        if (commentsArray != nil) {
            NSMutableArray *comments = [NSMutableArray arrayWithCapacity:commentsArray.count];
            for (NSDictionary *commentInfoDict in commentsArray) {
                PDCComment *comment       = [PDCComment new]; // TODO: 级联回复得处理
                comment.commentId         = commentInfoDict[@"id"];
                comment.content           = commentInfoDict[@"content"];
                comment.createTimestamp   = commentInfoDict[@"create_time_ms"];
                comment.createTimeStr     = commentInfoDict[@"create_time"];
                comment.canDel            = YES; //[commentInfoDict[@"deleted"] boolValue];
                NSDictionary *userInfoDic = commentInfoDict[@"user"];
                comment.createUser        = [self parseUserInfo:userInfoDic];
                [comments addObject:comment];
            }
            moment.comments = comments;
        }
        NSDictionary *userInfoDict = momentItemDict[@"create_user"];
        moment.createUser = [self parseUserInfo:userInfoDict];
        [resultMoments addObject:moment];
    }
    
    return resultMoments;
}

- (PDCUser *)parseUserInfo:(NSDictionary *)userInfoDict {
    PDCUser *createdUser = [PDCUser new];
    createdUser.userId = userInfoDict[@"user_id"];
    createdUser.nickName = userInfoDict[@"nick_name"];
    createdUser.avatarImageUrl = userInfoDict[@"image_url"];
    return createdUser;
}

#pragma mark -
#pragma mark - Control

- (void)showSystemCameraPicker {
    UIImagePickerController *imagePickerVc =  [UIImagePickerController new];
    imagePickerVc.allowsEditing = YES;
    imagePickerVc.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePickerVc.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    [self.navigationController presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)showPhotoSelectPicker {
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 delegate:nil]; // github 搜索 '图片选择器', start 最多的
    [imagePickerVc setDidFinishPickingPhotosWithInfosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto, NSArray<NSDictionary *> *infos) {
        switch (self.pickMediaDataMode) {
            case ForNewMoment:
            {
                [self navigateToPublishViewController:photos];
            }
                break;
            case ForNewCover:
            {
                
            }
            default:
                break;
        }

    }];
    [self.navigationController presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)navigateToPublishViewController:(NSArray<UIImage *> *)photos {
    PDCPublishViewController *publishViewController = [[PDCPublishViewController alloc] initWithImages:photos];
    [self.navigationController pushViewController:publishViewController animated:YES];
    self.publishCoordinator = [[PublishCoordinator alloc] initWithPublishViewController:publishViewController];
}

- (void)navigateToMineTimeline {
    PDCMomentsViewController *mineOwnTimelineViewController = [PDCMomentsViewController new];
    mineOwnTimelineViewController.containsInTabController = NO;
    mineOwnTimelineViewController.showMode = MineOwn;
    [self.navigationController pushViewController:mineOwnTimelineViewController animated:YES];
    self.mineOwnTimelineCoordinator = [[DiscoveryCoordinator alloc] initWithMomentsViewController:mineOwnTimelineViewController];
}

#pragma mark -
#pragma mark - Util

- (UINavigationController *)navigationController {
    return self.momentsViewController.navigationController;
}

@end
