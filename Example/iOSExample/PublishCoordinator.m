//
//  PublishCoordinator.m
//  iOSExample
//
//  Created by 孙燕飞 on 2017/11/28.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import "PublishCoordinator.h"
#import "TZImagePickerController.h"

@interface PublishCoordinator()<PDCPublishViewControllerDelegate>

@property (nonatomic, strong) UINavigationController *navigationController;
@property (nonatomic, strong) PDCPublishViewController *publishViewController;

@end

@implementation PublishCoordinator

- (instancetype)initWithPublishViewController:(PDCPublishViewController *)publishViewController {
    self = [super init];
    if (self) {
        self.publishViewController = publishViewController;
        self.publishViewController.delegate = self;
        self.navigationController = publishViewController.navigationController;
    }
    return self;
}

#pragma mark - PDCPublishViewControllerDelegate

- (void)publishCurrentMomentWith:(NSString *)content andImages:(NSArray *)images {
    NSLog(@"the published content is :%@ and publish image count is :%@", content, @(images.count));
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addNewImageToCurrentMoment {
    TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithMaxImagesCount:9 delegate:nil]; // github 搜索 '图片选择器', start 最多的
    [imagePickerVc setDidFinishPickingPhotosWithInfosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto, NSArray<NSDictionary *> *infos) {
        NSLog(@"the final photo is %@", photos);
        NSLog(@"the picked info is : %@", infos);
        [self.publishViewController addNewImages: photos];
    }];
    [self.navigationController presentViewController:imagePickerVc animated:YES completion:nil];
}

- (void)showNearbyLocation {
    NSLog(@"定位功能...");
    [self.publishViewController updateLocation:@"北京市"];
}

@end
