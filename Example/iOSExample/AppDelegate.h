//
//  AppDelegate.h
//  iOSExample
//
//  Created by CoderAFI on 11/16/2017.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

