//
//  DiscoveryCoordinator.h
//  iOSExample
//
//  Created by 孙燕飞 on 2017/11/28.
//  Copyright © 2017年 Cross. All rights reserved.
//

#import <FriendsCircle/FriendsCircle.h>

@interface DiscoveryCoordinator : NSObject

- (instancetype)initWithMomentsViewController:(PDCMomentsViewController *)momentsViewController;

@end
